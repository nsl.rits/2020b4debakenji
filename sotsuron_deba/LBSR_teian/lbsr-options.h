/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2011 Yufei Cheng
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Yufei Cheng   <yfcheng@ittc.ku.edu>
 *
 * James P.G. Sterbenz <jpgs@ittc.ku.edu>, director
 * ResiliNets Research Group  http://wiki.ittc.ku.edu/resilinets
 * Information and Telecommunication Technology Center (ITTC)
 * and Department of Electrical Engineering and Computer Science
 * The University of Kansas Lawrence, KS USA.
 *
 * Work supported in part by NSF FIND (Future Internet Design) Program
 * under grant CNS-0626918 (Postmodern Internet Architecture),
 * NSF grant CNS-1050226 (Multilayer Network Resilience Analysis and Experimentation on GENI),
 * US Department of Defense (DoD), and ITTC at The University of Kansas.
 */

#ifndef LBSR_OPTION_H
#define LBSR_OPTION_H

#include <map>
#include <list>

#include "ns3/buffer.h"
#include "ns3/packet.h"
#include "ns3/callback.h"
#include "ns3/ptr.h"
#include "ns3/udp-l4-protocol.h"
#include "ns3/ipv4.h"
#include "ns3/ipv4-route.h"
#include "ns3/object.h"
#include "ns3/node.h"
#include "ns3/ipv4-interface.h"
#include "ns3/ipv4-header.h"
#include "ns3/ipv4-address.h"
#include "ns3/traced-callback.h"
#include "ns3/output-stream-wrapper.h"
#include "ns3/timer.h"

#include "lbsr-rsendbuff.h"
#include "lbsr-maintain-buff.h"
#include "lbsr-option-header.h"
#include "lbsr-rcache.h"
#include "lbsr-routing.h"
#include "lbsr-gratuitous-reply-table.h"

namespace ns3 {

class Packet;
class NetDevice;
class Node;
class Ipv4Address;
class Ipv4Interface;
class Ipv4Route;
class Ipv4;
class Time;

namespace lbsr {

class LbsrOptions : public Object
{

public:


	std::vector<EventId> m_eventVector;


  /**
   * \brief Get the type identificator.
   * \return type identificator
   */

		static bool Lreq;
		void SetFlag(bool f){
			Lreq = f;
		};
		bool GetFlag(){
			return Lreq;
		};

	class SaveReq{
	public:
		static std::map<int, std::vector<LbsrRoutingHeader>> saveHeadermap;
		static std::map<int, std::vector<Ipv4Address>> saveAddressmap;
		static std::map<int, std::vector<uint8_t>> saveTtlmap;
		SaveReq(){};
		void PushBackSaveReq(int i, LbsrRoutingHeader hd, Ipv4Address ad, uint8_t ttl){
			saveHeadermap[i].push_back(hd);
			saveAddressmap[i].push_back(ad);
			saveTtlmap[i].push_back(ttl);
		};

		LbsrRoutingHeader GetHeader(int num, int i){
			return saveHeadermap[num][i];
		}



		Ipv4Address GetIpv4Address(int num, int i){
			return saveAddressmap[num][i];
		};

		uint8_t GetTtl(int num, int i){
			return saveTtlmap[num][i];
		};

		int GetVectorSize(int i){
			return (int)saveAddressmap[i].size();
		};

		void ClearVector(int i){
			saveHeadermap[i].clear();
			std::vector<LbsrRoutingHeader>().swap(saveHeadermap[i]);
			saveAddressmap[i].clear();
			std::vector<Ipv4Address>().swap(saveAddressmap[i]);
			saveTtlmap[i].clear();
			std::vector<uint8_t>().swap(saveTtlmap[i]);
		};
	};



	bool Dstflag;

	int m_addr;
  static TypeId GetTypeId (void);
  /**
   * \brief Constructor.
   */
  LbsrOptions ();
  /**
   * \brief Destructor.
   */

  Time ActiveRouteTimeout;

  Time GetActiveRouteTimeout(){
	  return ActiveRouteTimeout;
  }

    static std::vector<std::vector<Ipv4Address>> SaveRoute;
    static bool CreateFlag;

    std::vector<Ipv4Address> GetSaveRoute(void){
    	std::vector<Ipv4Address> tempRoute;
    	tempRoute = SaveRoute.front();
    	SaveRoute.erase(SaveRoute.begin());
    	return tempRoute;
    }

    void PushBackSaveRoute(std::vector<Ipv4Address> ro){
    	SaveRoute.push_back(ro);
    }

    int CheckSaveRoute(){
    	return (int)SaveRoute.size();
    }

	static std::vector<bool> req_flag;
	static std::vector<bool> stop_flag;
	static std::vector<bool> conf_flag;
	//std::map<Ipv4Address, int> nextMap;
	static std::vector<std::map<Ipv4Address, int>> next;
	static std::vector<int> hops;
	static std::vector<int> addr_num;
	static std::vector<std::vector<Ipv4Address>> nodesRoute;

void PushBackNodeCache(bool r, bool s, bool c, Ipv4Address ne, int h, int a){
	//m_nodeCache tempCache = {r, s, c, ne, h, a};
	//nodeCache.push_back(tempCache);
	req_flag.push_back(r);
	stop_flag.push_back(s);
	conf_flag.push_back(c);
	std::map<Ipv4Address, int> nextMap;
	nextMap.insert(std::make_pair(ne, 0));
	next.push_back(nextMap);//こいつのpush_backはここじゃなくてもいいかも
	hops.push_back(h);
	addr_num.push_back(a);
	std::vector<Ipv4Address> tempRoute;
	tempRoute.push_back("0.0.0.0");
	nodesRoute.push_back(tempRoute);
};

void SetNodeCache(int i, bool r, bool s, bool c, Ipv4Address ne, int h, int a){
	req_flag[i] = r;
	stop_flag[i] = s;
	conf_flag[i] = c;
	std::map<Ipv4Address, int> nextMap;
	if(ne != "0.0.0.0"){
		nextMap = next[i];
		decltype(nextMap)::iterator it = nextMap.find(ne);
		if(it != nextMap.end()){
			nextMap[ne] = nextMap[ne]+1;
		}else{
			nextMap.insert(std::make_pair(ne, 1));
		}
	}else{
		next[i].clear();
		std::map<Ipv4Address, int>().swap(next[i]);
		nextMap.insert(std::make_pair(ne, 0));
	}
	next[i] = nextMap;
	hops[i] = h;
	addr_num[i] = a;
}

void SetNodeCache_ButNext(int i, bool r, bool s, bool c, int h, int a){
	req_flag[i] = r;
	stop_flag[i] = s;
	conf_flag[i] = c;
	hops[i] = h;
	addr_num[i] = a;
}

void SetStopFlag(int i, bool s){
	stop_flag[i] = s;
};

void SetNodesRoute(int i, std::vector<Ipv4Address> route){
	nodesRoute[i]=route;
}

std::vector<Ipv4Address> GetNodesRoute(int i){
	if(nodesRoute[i].size() < 3){
		std::vector<Ipv4Address> temp;
		temp.push_back("0.0.0.0");
		return temp;
	}else{
		return nodesRoute[i];
	}
};

bool GetReqFlag(int i){
	//return nodeCache[i].req_flag;
	return req_flag[i];
};

bool GetStopFlag(int i){
	//return nodeCache[i].stop_flag;
	return stop_flag[i];
};

bool GetConfFlag(int i){
	//return nodeCache[i].conf_flag;
	return conf_flag[i];
};

std::map<Ipv4Address, int> GetNext(int i){
	//return nodeCache[i].next;
	if(i+1 > (int)next.size()){
		std::map<Ipv4Address, int> tempMap;
		tempMap.insert(std::make_pair("0.0.0.0", 0));
		return tempMap;
	}else{
		return next[i];
	}
};

std::vector<Ipv4Address> GetSendLreqNext(int i){
	if(i+1 > (int)nodesRoute.size()){
		std::vector<Ipv4Address> nextAddresses;
		nextAddresses.push_back("0.0.0.0");
		return  nextAddresses;
	}else{
		if((int)nodesRoute[i].size() >= 3){//今回のルートにdestinationがないが以前にLconfを受け取っている(ノードがルートを保存している)時
			int maxCount=0;
			Ipv4Address ne;
			std::map<Ipv4Address, int>tempmap;
			std::vector<Ipv4Address> nextAddresses;
			nextAddresses.clear();
			tempmap = next[i];

			for(std::map<Ipv4Address, int>::const_iterator j = tempmap.begin(); j != tempmap.end(); ++j){
				if(maxCount < tempmap[j->first]){
					nextAddresses.clear();
					maxCount = tempmap[j->first];
					ne = j->first;
					nextAddresses.push_back(ne);
				}else if(maxCount == tempmap[j->first]){
					ne = j->first;
					//nextAddresses.push_back(ne);
				}
			}
			return nextAddresses;
		}else if((int)nodesRoute[i].size() < 3){//ルートにdestinationがなく、まだLconfを受け取っていない(ノードがルートを保存していない)時
			std::vector<Ipv4Address> nextAddresses;
			nextAddresses.push_back("0.0.0.0");
			return  nextAddresses;
		}
	}
	std::vector<Ipv4Address> nextAddresses;
	nextAddresses.push_back("0.0.0.0");
	return  nextAddresses;
};

int GetHops(int i){
	//return nodeCache[i].hops;
	return hops[i];
};

int GetAddr(int i){
	//return nodeCache[i].addr_num;
	return addr_num[i];
};

void ClearNodeCache(void){
	req_flag.clear();
	std::vector<bool>().swap(req_flag);
	stop_flag.clear();
	std::vector<bool>().swap(stop_flag);
	conf_flag.clear();
	std::vector<bool>().swap(conf_flag);
	next.clear();
	std::vector<std::map<Ipv4Address, int>>().swap(next);
	hops.clear();
	std::vector<int>().swap(hops);
	addr_num.clear();
	std::vector<int>().swap(addr_num);
	nodesRoute.clear();
	std::vector<std::vector<Ipv4Address>>().swap(nodesRoute);
};

static bool shceflag;

void Setshceflag(bool flag){
	shceflag=flag;
};

bool Getshceflag(void){
	return shceflag;
};



static std::vector<std::vector<Ipv4Address>> RouteNodeList;
static std::vector<int> saveAddr;
static std::vector<uint8_t> saveProtocol;
static std::vector<Ipv4Address> saveAddress;
static std::vector<uint8_t> saveNumAddr;
static std::vector<double> saveBattery;

void SetRouteNodeList(std::vector<Ipv4Address> list, int addr, uint8_t pro, Ipv4Address savead, uint8_t num, double battery){
	RouteNodeList.push_back(list);
	saveAddr.push_back(addr);
	saveProtocol.push_back(pro);
	saveAddress.push_back(savead);
	saveNumAddr.push_back(num);
	saveBattery.push_back(battery);
};

double GetSaveBattery(int i){
	return saveBattery[i];
}
std::vector<Ipv4Address> GetRouteNodeList(int i){
	return RouteNodeList[i];
};

int GetSaveAddr(int i){
	return saveAddr[i];
};

uint8_t GetSaveProtocol(int i){
	return saveProtocol[i];
};

Ipv4Address GetSaveAddress(int i){
	return saveAddress[i];
};

uint8_t GetSaveNumAddr(int i){
	return saveNumAddr[i];
};

int GetSaveSize(){
	return (int)saveAddress.size();
};

void ClearRouteNodeList(void){
	RouteNodeList.clear();
	std::vector<std::vector<Ipv4Address>>().swap(RouteNodeList);
	saveAddr.clear();
	std::vector<int>().swap(saveAddr);
	saveProtocol.clear();
	std::vector<uint8_t>().swap(saveProtocol);
	saveAddress.clear();
	std::vector<Ipv4Address>().swap(saveAddress);
	saveNumAddr.clear();
	std::vector<uint8_t>().swap(saveNumAddr);
};

static std::vector<Ipv4Address> CreateRoute;

void SetCreateRoute(std::vector<Ipv4Address > add){
	CreateRoute=add;
};

std::vector<Ipv4Address> GetCreateRoute(void){
	return CreateRoute;
};

void SendRouteNode();
	//Send Created Route when called by shcedule

  virtual ~LbsrOptions ();
  /**
    * \brief Get the option number.
    * \return option number
    */
  virtual uint8_t GetOptionNumber () const = 0;
  /**
   * \brief Set the node.
   * \param node the node to set
   */



  void SetNode (Ptr<Node> node);
  /**
   * \brief Get the node.
   * \return the node
   */
  Ptr<Node> GetNode () const;
  /**
   * \brief Search for the ipv4 address in the node list.
   *
   * \param ipv4Address IPv4 address to search for
   * \param destAddress IPv4 address in the list that we begin the search
   * \param nodeList List of IPv4 addresses
   * \return true if contain ip address
   */
  bool ContainAddressAfter (Ipv4Address ipv4Address, Ipv4Address destAddress, std::vector<Ipv4Address> &nodeList);
  /**
   * \brief Cut the route from ipv4Address to the end of the route vector
   *
   * \param ipv4Address the address to begin cutting
   * \param nodeList List of IPv4 addresses
   * \return the vector after the route cut
   */
  std::vector<Ipv4Address> CutRoute (Ipv4Address ipv4Address, std::vector<Ipv4Address> &nodeList);
  /**
   * \brief Set the route to use for data packets,
   *        used by the option headers when sending data/control packets
   *
   * \param nextHop IPv4 address of the next hop
   * \param srcAddress IPv4 address of the source
   * \return the route
   */
  virtual Ptr<Ipv4Route> SetRoute (Ipv4Address nextHop, Ipv4Address srcAddress);
  /**
   * \brief Reverse the routes.
   *
   * \param vec List of IPv4 addresses
   * \return true if successfully reversed
   */
  bool ReverseRoutes  (std::vector<Ipv4Address>& vec);
  /**
   * \brief Search for the next hop in the route
   *
   * \param ipv4Address the IPv4 address of the node we are looking for its next hop address
   * \param vec List of IPv4 addresses
   * \return the next hop address if found
   */
  Ipv4Address SearchNextHop (Ipv4Address ipv4Address, std::vector<Ipv4Address>& vec);
  /**
   * \brief Reverse search for the next hop in the route
   *
   * \param ipv4Address the IPv4 address of the node we are looking for its next hop address
   * \param vec List of IPv4 addresses
   * \return the previous next hop address if found
   */
  Ipv4Address ReverseSearchNextHop (Ipv4Address ipv4Address, std::vector<Ipv4Address>& vec);
  /**
   * \brief Reverse search for the next two hop in the route
   *
   * \param ipv4Address the IPv4 address of the node we are looking for its next two hop address
   * \param vec List of IPv4 addresses
   * \return the previous next two hop address if found
   */
  Ipv4Address ReverseSearchNextTwoHop  (Ipv4Address ipv4Address, std::vector<Ipv4Address>& vec);
  /**
   * \brief Print out the elements in the route vector
   * \param vec The route vector to print.
   */
  void PrintVector (std::vector<Ipv4Address>& vec);
  /**
   * \brief Check if the two vectors contain duplicate or not
   *
   * \param vec the first list of IPv4 addresses
   * \param vec2 the second list of IPv4 addresses
   * \return true if contains duplicate
   */
  bool IfDuplicates (std::vector<Ipv4Address>& vec, std::vector<Ipv4Address>& vec2);
  /**
   * \brief Check if the route already contains the node ip address
   *
   * \param ipv4Address the IPv4 address that we are looking for
   * \param vec List of IPv4 addresses
   * \return true if it already exists
   */
  bool CheckDuplicates (Ipv4Address ipv4Address, std::vector<Ipv4Address>& vec);
  /**
   * \brief Remove the duplicates from the route
   *
   * \param vec List of IPv4 addresses to be clean
   * \return the route after route shorten
   */
  void RemoveDuplicates (std::vector<Ipv4Address>& vec);
  /**
   * \brief Schedule the intermediate node route request broadcast
   * \param packet the original packet
   * \param nodeList The list of IPv4 addresses
   * \param source address
   * \param destination address
   */
  void ScheduleReply (Ptr<Packet> &packet, std::vector<Ipv4Address> &nodeList, Ipv4Address &source, Ipv4Address &destination);
  /**
   * \brief Get the node id with Ipv4Address
   *
   * \param address IPv4 address to look for ID
   * \return the id of the node
   */
  uint32_t GetIDfromIP (Ipv4Address address);
  /**
   * \brief Get the node object with Ipv4Address
   *
   * \param ipv4Address IPv4 address of the node
   * \return the object of the node
   */
  Ptr<Node> GetNodeWithAddress (Ipv4Address ipv4Address);
  /**
   * \brief Process method
   *
   * Called from LbsrRouting::Receive.
   * \param packet the packet
   * \param lbsrP  the clean packet with payload
   * \param ipv4Address the IPv4 address
   * \param source IPv4 address of the source
   * \param ipv4Header the IPv4 header of packet received
   * \param protocol the protocol number of the up layer
   * \param isPromisc if the packet must be dropped
   * \param promiscSource IPv4 address
   * \return the processed size
   */
  virtual uint8_t Process (Ptr<Packet> packet, Ptr<Packet> lbsrP, Ipv4Address ipv4Address, Ipv4Address source, Ipv4Header const& ipv4Header, uint8_t protocol, bool& isPromisc, Ipv4Address promiscSource) = 0;

protected:
  /**
   * \brief Drop trace callback.
   */
  TracedCallback<Ptr<const Packet> > m_dropTrace;
  /**
   * \brief The broadcast IP address.
   */
  Ipv4Address Broadcast;
  /**
   * \brief The route request table.
   */
  Ptr<lbsr::LbsrRreqTable> m_rreqTable;
  /**
   * \brief The route cache table.
   */
  Ptr<lbsr::LbsrRouteCache> m_routeCache;
  /**
   * \brief The ipv4 route.
   */
  Ptr<Ipv4Route> m_ipv4Route;
  /**
   * \brief The ipv4.
   */
  Ptr<Ipv4> m_ipv4;
  /**
   * \brief The vector of Ipv4 address.
   */
  std::vector<Ipv4Address> m_ipv4Address;

  std::vector<Ipv4Address> m_finalRoute;

  /**
   * \brief The active route timeout value.
   */
  //Time ActiveRouteTimeout;
  /**
   * The receive trace back, only triggered when final destination receive data packet
   */
  TracedCallback <const LbsrOptionSRHeader &> m_rxPacketTrace;

  bool m_DstFlag;

	//std::vector<Ptr<Packet>> savePacket;
	//std::vector<LbsrOptionRreqHeader> saveRreq;
	//std::vector<Ipv4Address> saveIpv4Address;

private:
  Ptr<Node> m_node; ///< the node
};

/**
 * \class LbsrOptionPad1
 * \brief Lbsr Option Pad1
 */
class LbsrOptionPad1 : public LbsrOptions
{
public:
  /**
   * \brief Pad1 option number.
   */
  static const uint8_t OPT_NUMBER = 224;

  /**
   * \brief Get the type ID.
   * \return the object TypeId
   */
  static TypeId GetTypeId ();

  LbsrOptionPad1 ();
  virtual ~LbsrOptionPad1 ();

  virtual uint8_t GetOptionNumber () const;
  virtual uint8_t Process (Ptr<Packet> packet, Ptr<Packet> lbsrP, Ipv4Address ipv4Address, Ipv4Address source, Ipv4Header const& ipv4Header, uint8_t protocol, bool& isPromisc, Ipv4Address promiscSource);
};

/**
 * \class LbsrOptionPadn
 * \brief IPv4 Option Padn
 */
class LbsrOptionPadn : public LbsrOptions
{
public:
  /**
   * \brief PadN option number.
   */
  static const uint8_t OPT_NUMBER = 0;

  /**
   * \brief Get the type ID.
   * \return the object TypeId
   */
  static TypeId GetTypeId ();

  LbsrOptionPadn ();
  virtual ~LbsrOptionPadn ();

  virtual uint8_t GetOptionNumber () const;
  virtual uint8_t Process (Ptr<Packet> packet, Ptr<Packet> lbsrP, Ipv4Address ipv4Address, Ipv4Address source, Ipv4Header const& ipv4Header, uint8_t protocol, bool& isPromisc, Ipv4Address promiscSource);
};

/**
 * \class LbsrOptionRreq
 * \brief Lbsr Option Rreq
 */

class LbsrOptionRreq : public LbsrOptions
{
public:
  /**
   * \brief Rreq option number.
   */
  static const uint8_t OPT_NUMBER = 1;

  /**
   * \brief Get the type ID.
   * \return the object TypeId
   */
  static TypeId GetTypeId ();
  /**
   * \brief Get the instance type ID.
   * \return instance type ID
   */
  virtual TypeId GetInstanceTypeId () const;
  /**
   * \brief Constructor.
   */
  LbsrOptionRreq ();
  /**
   * \brief Destructor.
   */
  virtual ~LbsrOptionRreq ();

  virtual uint8_t GetOptionNumber () const;
  virtual uint8_t Process (Ptr<Packet> packet, Ptr<Packet> lbsrP, Ipv4Address ipv4Address, Ipv4Address source, Ipv4Header const& ipv4Header, uint8_t protocol, bool& isPromisc, Ipv4Address promiscSource);

private:
  /**
   * \brief The route cache.
   */
  Ptr<lbsr::LbsrRouteCache> m_routeCache;
  /**
   * \brief The ipv4.
   */
  Ptr<Ipv4> m_ipv4;
};

/**
 * \class LbsrOptionRrep
 * \brief Lbsr Option Route Reply
 */
class LbsrOptionRrep : public LbsrOptions
{
public:
  /**
   * \brief Router alert option number.
   */
  static const uint8_t OPT_NUMBER = 2;

  /**
   * \brief Get the type ID.
   * \return the object TypeId
   */
  static TypeId GetTypeId ();
  /**
   * \brief Get the instance type ID.
   * \return instance type ID
   */
  virtual TypeId GetInstanceTypeId () const;

  LbsrOptionRrep ();
  virtual ~LbsrOptionRrep ();

  virtual uint8_t GetOptionNumber () const;
  virtual uint8_t Process (Ptr<Packet> packet, Ptr<Packet> lbsrP, Ipv4Address ipv4Address, Ipv4Address source, Ipv4Header const& ipv4Header, uint8_t protocol, bool& isPromisc, Ipv4Address promiscSource);

private:
  /**
   * \brief The route cache.
   */
  Ptr<lbsr::LbsrRouteCache> m_routeCache;
  /**
   * \brief The ip layer 3.
   */
  Ptr<Ipv4> m_ipv4;

};

/**
 * \class LbsrOptionSR
 * \brief Lbsr Option Source Route
 */

//Lstop
class LbsrOptionLstop : public LbsrOptions
{
public:
  /**
   * \brief Rreq option number.
   */
  static const uint8_t OPT_NUMBER = 5;

  /**
   * \brief Get the type ID.
   * \return the object TypeId
   */
  static TypeId GetTypeId ();
  /**
   * \brief Get the instance type ID.
   * \return instance type ID
   */
  virtual TypeId GetInstanceTypeId () const;
  /**
   * \brief Constructor.
   */
  LbsrOptionLstop ();
  /**
   * \brief Destructor.
   */
  virtual ~LbsrOptionLstop ();

  virtual uint8_t GetOptionNumber () const;
  virtual uint8_t Process (Ptr<Packet> packet, Ptr<Packet> lbsrP, Ipv4Address ipv4Address, Ipv4Address source, Ipv4Header const& ipv4Header, uint8_t protocol, bool& isPromisc, Ipv4Address promiscSource);

private:
  /**
   * \brief The route cache.
   */
  Ptr<lbsr::LbsrRouteCache> m_routeCache;
  /**
   * \brief The ipv4.
   */
  Ptr<Ipv4> m_ipv4;
};


class LbsrOptionSR : public LbsrOptions
{
public:
  /**
   * \brief Source Route option number.
   */
  static const uint8_t OPT_NUMBER = 96;

  /**
   * \brief Get the type ID.
   * \return the object TypeId
   */
  static TypeId GetTypeId ();
  /**
   * \brief Get the instance type ID.
   * \return instance type ID
   */
  virtual TypeId GetInstanceTypeId () const;

  LbsrOptionSR ();
  virtual ~LbsrOptionSR ();

  virtual uint8_t GetOptionNumber () const;
  virtual uint8_t Process (Ptr<Packet> packet, Ptr<Packet> lbsrP, Ipv4Address ipv4Address, Ipv4Address source, Ipv4Header const& ipv4Header, uint8_t protocol, bool& isPromisc, Ipv4Address promiscSource);

private:
  /**
   * \brief The ip layer 3.
   */
  Ptr<Ipv4> m_ipv4;
};

/**
 * \class LbsrOptionRerr
 * \brief Lbsr Option Route Error
 */
class LbsrOptionRerr : public LbsrOptions
{
public:
  /**
   * \brief Lbsr Route Error option number.
   */
  static const uint8_t OPT_NUMBER = 3;

  /**
   * \brief Get the type ID.
   * \return the object TypeId
   */
  static TypeId GetTypeId ();
  /**
   * \brief Get the instance type ID.
   * \return instance type ID
   */
  virtual TypeId GetInstanceTypeId () const;

  LbsrOptionRerr ();
  virtual ~LbsrOptionRerr ();

  virtual uint8_t GetOptionNumber () const;
  virtual uint8_t Process (Ptr<Packet> packet, Ptr<Packet> lbsrP, Ipv4Address ipv4Address, Ipv4Address source, Ipv4Header const& ipv4Header, uint8_t protocol, bool& isPromisc, Ipv4Address promiscSource);
  /**
   * \brief Do Send error message
   *
   * \param p the packet
   * \param rerr  the LbsrOptionRerrUnreachHeader header
   * \param rerrSize the route error header size
   * \param ipv4Address ipv4 address of our own
   * \param protocol the protocol number of the up layer
   * \return the processed size
   */
  uint8_t DoSendError (Ptr<Packet> p, LbsrOptionRerrUnreachHeader &rerr, uint32_t rerrSize, Ipv4Address ipv4Address, uint8_t protocol);

private:
  /**
   * \brief The route cache.
   */
  Ptr<lbsr::LbsrRouteCache> m_routeCache;
  /**
   * \brief The ipv4 layer 3.
   */
  Ptr<Ipv4> m_ipv4;
};

/**
 * \class LbsrOptionAckReq
 * \brief Lbsr Option
 */
class LbsrOptionAckReq : public LbsrOptions
{
public:
  /**
   * \brief Lbsr ack request option number.
   */
  static const uint8_t OPT_NUMBER = 160;

  /**
   * \brief Get the type ID.
   * \return the object TypeId
   */
  static TypeId GetTypeId ();
  /**
   * \brief Get the instance type ID.
   * \return instance type ID
   */
  virtual TypeId GetInstanceTypeId () const;

  LbsrOptionAckReq ();
  virtual ~LbsrOptionAckReq ();

  virtual uint8_t GetOptionNumber () const;
  virtual uint8_t Process (Ptr<Packet> packet, Ptr<Packet> lbsrP, Ipv4Address ipv4Address, Ipv4Address source, Ipv4Header const& ipv4Header, uint8_t protocol, bool& isPromisc, Ipv4Address promiscSource);

private:
  /**
   * \brief The route cache.
   */
  Ptr<lbsr::LbsrRouteCache> m_routeCache;
  /**
   * \brief The ipv4 layer 3.
   */
  Ptr<Ipv4> m_ipv4;

  bool m_DstFlag;
};

/**
 * \class LbsrOptionAck
 * \brief Lbsr Option Ack
 */
class LbsrOptionAck : public LbsrOptions
{
public:
  /**
   * \brief The Lbsr Ack option number.
   */
  static const uint8_t OPT_NUMBER = 32;

  /**
   * \brief Get the type ID.
   * \return the object TypeId
   */
  static TypeId GetTypeId ();
  /**
   * \brief Get the instance type ID.
   * \return instance type ID
   */
  virtual TypeId GetInstanceTypeId () const;

  LbsrOptionAck ();
  virtual ~LbsrOptionAck ();

  virtual uint8_t GetOptionNumber () const;
  virtual uint8_t Process (Ptr<Packet> packet, Ptr<Packet> lbsrP, Ipv4Address ipv4Address, Ipv4Address source, Ipv4Header const& ipv4Header, uint8_t protocol, bool& isPromisc, Ipv4Address promiscSource);

private:
  /**
   * \brief The route cache.
   */
  Ptr<lbsr::LbsrRouteCache> m_routeCache;
  /**
   * \brief The ipv4 layer 3.
   */
  Ptr<Ipv4> m_ipv4;
};
} // namespace lbsr
} // Namespace ns3

#endif
