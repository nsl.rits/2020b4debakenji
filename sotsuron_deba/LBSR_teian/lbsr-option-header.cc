/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2011 Yufei Cheng
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Yufei Cheng   <yfcheng@ittc.ku.edu>
 *
 * James P.G. Sterbenz <jpgs@ittc.ku.edu>, director
 * ResiliNets Research Group  http://wiki.ittc.ku.edu/resilinets
 * Information and Telecommunication Technology Center (ITTC)
 * and Department of Electrical Engineering and Computer Science
 * The University of Kansas Lawrence, KS USA.
 *
 * Work supported in part by NSF FIND (Future Internet Design) Program
 * under grant CNS-0626918 (Postmodern Internet Architecture),
 * NSF grant CNS-1050226 (Multilayer Network Resilience Analysis and Experimentation on GENI),
 * US Department of Defense (DoD), and ITTC at The University of Kansas.
 */

#include "ns3/assert.h"
#include "ns3/log.h"
#include "ns3/header.h"
#include "lbsr-option-header.h"
#include "ns3/ipv4-address.h"
#include "ns3/address-utils.h"
#include "ns3/packet.h"
#include "ns3/enum.h"

namespace ns3 {

NS_LOG_COMPONENT_DEFINE ("LbsrOptionHeader");

namespace lbsr {

NS_OBJECT_ENSURE_REGISTERED (LbsrOptionHeader);

TypeId LbsrOptionHeader::GetTypeId ()
{
  static TypeId tid = TypeId ("ns3::lbsr::LbsrOptionHeader")
    .AddConstructor<LbsrOptionHeader> ()
    .SetParent<Header> ()
    .SetGroupName ("Lbsr")
  ;
  return tid;
}

TypeId LbsrOptionHeader::GetInstanceTypeId () const
{
  return GetTypeId ();
}

LbsrOptionHeader::LbsrOptionHeader ()
  : m_type (0),
    m_length (0)
{
}

LbsrOptionHeader::~LbsrOptionHeader ()
{
}

void LbsrOptionHeader::SetType (uint8_t type)
{
  m_type = type;
}

uint8_t LbsrOptionHeader::GetType () const
{
  return m_type;
}

void LbsrOptionHeader::SetLength (uint8_t length)
{
  m_length = length;
}

uint8_t LbsrOptionHeader::GetLength () const
{
  return m_length;
}

void LbsrOptionHeader::Print (std::ostream &os) const
{
  os << "( type = " << (uint32_t)m_type << " length = " << (uint32_t)m_length << " )";
}

uint32_t LbsrOptionHeader::GetSerializedSize () const
{
  return m_length + 2;
}

void LbsrOptionHeader::Serialize (Buffer::Iterator start) const
{
  Buffer::Iterator i = start;

  i.WriteU8 (m_type);
  i.WriteU8 (m_length);
  i.Write (m_data.Begin (), m_data.End ());
}

uint32_t LbsrOptionHeader::Deserialize (Buffer::Iterator start)
{
  Buffer::Iterator i = start;

  m_type = i.ReadU8 ();
  m_length = i.ReadU8 ();

  m_data = Buffer ();
  m_data.AddAtEnd (m_length);
  Buffer::Iterator dataStart = i;
  i.Next (m_length);
  Buffer::Iterator dataEnd = i;
  m_data.Begin ().Write (dataStart, dataEnd);

  return GetSerializedSize ();
}

LbsrOptionHeader::Alignment LbsrOptionHeader::GetAlignment () const
{
  Alignment retVal = { 1, 0 };
  return retVal;
}

NS_OBJECT_ENSURE_REGISTERED (LbsrOptionPad1Header);

TypeId LbsrOptionPad1Header::GetTypeId ()
{
  static TypeId tid = TypeId ("ns3::lbsr::LbsrOptionPad1Header")
    .AddConstructor<LbsrOptionPad1Header> ()
    .SetParent<LbsrOptionHeader> ()
    .SetGroupName ("Lbsr")
  ;
  return tid;
}

TypeId LbsrOptionPad1Header::GetInstanceTypeId () const
{
  return GetTypeId ();
}

LbsrOptionPad1Header::LbsrOptionPad1Header ()
{
  SetType (224);
}

LbsrOptionPad1Header::~LbsrOptionPad1Header ()
{
}

void LbsrOptionPad1Header::Print (std::ostream &os) const
{
  os << "( type = " << (uint32_t)GetType () << " )";
}

uint32_t LbsrOptionPad1Header::GetSerializedSize () const
{
  return 1;
}

void LbsrOptionPad1Header::Serialize (Buffer::Iterator start) const
{
  Buffer::Iterator i = start;

  i.WriteU8 (GetType ());
}

uint32_t LbsrOptionPad1Header::Deserialize (Buffer::Iterator start)
{
  Buffer::Iterator i = start;

  SetType (i.ReadU8 ());

  return GetSerializedSize ();
}

NS_OBJECT_ENSURE_REGISTERED (LbsrOptionPadnHeader);

TypeId LbsrOptionPadnHeader::GetTypeId ()
{
  static TypeId tid = TypeId ("ns3::lbsr::LbsrOptionPadnHeader")
    .AddConstructor<LbsrOptionPadnHeader> ()
    .SetParent<LbsrOptionHeader> ()
    .SetGroupName ("Lbsr")
  ;
  return tid;
}

TypeId LbsrOptionPadnHeader::GetInstanceTypeId () const
{
  return GetTypeId ();
}

LbsrOptionPadnHeader::LbsrOptionPadnHeader (uint32_t pad)
{
  SetType (0);
  NS_ASSERT_MSG (pad >= 2, "PadN must be at least 2 bytes long");
  SetLength (pad - 2);
}

LbsrOptionPadnHeader::~LbsrOptionPadnHeader ()
{
}

void LbsrOptionPadnHeader::Print (std::ostream &os) const
{
  os << "( type = " << (uint32_t)GetType () << " length = " << (uint32_t)GetLength () << " )";
}

uint32_t LbsrOptionPadnHeader::GetSerializedSize () const
{
  return GetLength () + 2;
}

void LbsrOptionPadnHeader::Serialize (Buffer::Iterator start) const
{
  Buffer::Iterator i = start;

  i.WriteU8 (GetType ());
  i.WriteU8 (GetLength ());

  for (int padding = 0; padding < GetLength (); padding++)
    {
      i.WriteU8 (0);
    }
}

uint32_t LbsrOptionPadnHeader::Deserialize (Buffer::Iterator start)
{
  Buffer::Iterator i = start;

  SetType (i.ReadU8 ());
  SetLength (i.ReadU8 ());

  return GetSerializedSize ();
}

NS_OBJECT_ENSURE_REGISTERED (LbsrOptionRreqHeader);

TypeId LbsrOptionRreqHeader::GetTypeId ()
{
  static TypeId tid = TypeId ("ns3::lbsr::LbsrOptionRreqHeader")
    .AddConstructor<LbsrOptionRreqHeader> ()
    .SetParent<LbsrOptionHeader> ()
    .SetGroupName ("Lbsr")
  ;
  return tid;
}

TypeId LbsrOptionRreqHeader::GetInstanceTypeId () const
{
  return GetTypeId ();
}

LbsrOptionRreqHeader::LbsrOptionRreqHeader ()
  : m_ipv4Address (0)
{
  SetType (1);
  SetLength (6 + m_ipv4Address.size () * 4);
}

LbsrOptionRreqHeader::~LbsrOptionRreqHeader ()
{
}

void LbsrOptionRreqHeader::SetNumberAddress (uint8_t n)
{
  m_ipv4Address.clear ();
  m_ipv4Address.assign (n, Ipv4Address ());
}

Ipv4Address LbsrOptionRreqHeader::GetTarget ()
{
  return m_target;
}

void LbsrOptionRreqHeader::SetTarget (Ipv4Address target)
{
  m_target = target;
}

void LbsrOptionRreqHeader::AddNodeAddress (Ipv4Address ipv4)
{
  m_ipv4Address.push_back (ipv4);
  SetLength (6 + m_ipv4Address.size () * 4);
}

void LbsrOptionRreqHeader::SetNodesAddress (std::vector<Ipv4Address> ipv4Address)
{
  m_ipv4Address = ipv4Address;
  SetLength (6 + m_ipv4Address.size () * 4);
}

std::vector<Ipv4Address> LbsrOptionRreqHeader::GetNodesAddresses () const
{
  return m_ipv4Address;
}

uint32_t LbsrOptionRreqHeader::GetNodesNumber () const
{
  return m_ipv4Address.size ();
}

void LbsrOptionRreqHeader::SetNodeAddress (uint8_t index, Ipv4Address addr)
{
  m_ipv4Address.at (index) = addr;
}

Ipv4Address LbsrOptionRreqHeader::GetNodeAddress (uint8_t index) const
{
  return m_ipv4Address.at (index);
}

void LbsrOptionRreqHeader::SetId (uint16_t identification)
{
  m_identification = identification;
}

uint16_t LbsrOptionRreqHeader::GetId () const
{
  return m_identification;
}

void LbsrOptionRreqHeader::Print (std::ostream &os) const
{
  os << "( type = " << (uint32_t)GetType () << " length = " << (uint32_t)GetLength () << "";

  for (std::vector<Ipv4Address>::const_iterator it = m_ipv4Address.begin (); it != m_ipv4Address.end (); it++)
    {
      os << *it << " ";
    }

  os << ")";
}

uint32_t LbsrOptionRreqHeader::GetSerializedSize () const
{
  return 8 + m_ipv4Address.size () * 4;
}

void LbsrOptionRreqHeader::Serialize (Buffer::Iterator start) const
{
  Buffer::Iterator i = start;
  uint8_t buff[4];

  i.WriteU8 (GetType ());
  i.WriteU8 (GetLength ());
  i.WriteHtonU16 (m_identification);
  WriteTo (i, m_target);

  for (VectorIpv4Address_t::const_iterator it = m_ipv4Address.begin (); it != m_ipv4Address.end (); it++)
    {
      it->Serialize (buff);
      i.Write (buff, 4);
    }
}

uint32_t LbsrOptionRreqHeader::Deserialize (Buffer::Iterator start)
{
  Buffer::Iterator i = start;
  uint8_t buff[4];

  SetType (i.ReadU8 ());
  SetLength (i.ReadU8 ());
  m_identification = i.ReadNtohU16 ();
  ReadFrom (i, m_target);

  uint8_t index = 0;
  for (std::vector<Ipv4Address>::iterator it = m_ipv4Address.begin (); it != m_ipv4Address.end (); it++)
    {
      i.Read (buff, 4);
      m_address = it->Deserialize (buff);
      SetNodeAddress (index, m_address);
      ++index;
    }

  return GetSerializedSize ();
}

LbsrOptionHeader::Alignment LbsrOptionRreqHeader::GetAlignment () const
{
  Alignment retVal = { 4, 0 };
  return retVal;
}



//Rrep

//std::vector<double> LbsrOptionRrepHeader::nodeBattery;

NS_OBJECT_ENSURE_REGISTERED (LbsrOptionRrepHeader);

TypeId LbsrOptionRrepHeader::GetTypeId ()
{
  static TypeId tid = TypeId ("ns3::lbsr::LbsrOptionRrepHeader")
    .AddConstructor<LbsrOptionRrepHeader> ()
    .SetParent<LbsrOptionHeader> ()
    .SetGroupName ("Lbsr")
  ;
  return tid;
}

TypeId LbsrOptionRrepHeader::GetInstanceTypeId () const
{
  return GetTypeId ();
}

LbsrOptionRrepHeader::LbsrOptionRrepHeader ()
  : m_ipv4Address (0), nodeBattery(0)
{
  SetType (2);
  SetLength (2 + m_ipv4Address.size () * 4);
}

LbsrOptionRrepHeader::~LbsrOptionRrepHeader ()
{
}

void LbsrOptionRrepHeader::SetBattery(uint16_t battery)
{
	if(nodeBattery){
		if(battery < nodeBattery){
			nodeBattery=battery;
			//std::cout << "nodeBattery is Exist\n";
			//SetLength();
		}
	}else{
		nodeBattery=battery;
		//std::cout << "nodeBattery is NULL\n";
	}
}

void LbsrOptionRrepHeader::SetBatteryDe(uint16_t index){
	double battery= (double)index;
	//std::cout << "SetBatteryDe battery is "<< battery << "\n";
	nodeBattery= battery;
}

double LbsrOptionRrepHeader::GetBatter()
{
	return nodeBattery;
}

void LbsrOptionRrepHeader::SetAddrNum(uint8_t addr){
	addr_num = addr;
}

int LbsrOptionRrepHeader::GetAddrNum(){
	return addr_num;
}


uint64_t LbsrOptionRrepHeader::GetBatteryUint()
{
	uint64_t batt = (uint64_t)nodeBattery;
	return batt;
}

void LbsrOptionRrepHeader::SetNumberAddress (uint8_t n)
{
  m_ipv4Address.clear ();
  m_ipv4Address.assign (n, Ipv4Address ());
}

void LbsrOptionRrepHeader::SetNodesAddress (std::vector<Ipv4Address> ipv4Address)
{
  m_ipv4Address = ipv4Address;
  SetLength (2 + m_ipv4Address.size () * 4);
}

std::vector<Ipv4Address> LbsrOptionRrepHeader::GetNodesAddress () const
{
  return m_ipv4Address;
}

void LbsrOptionRrepHeader::SetNodeAddress (uint8_t index, Ipv4Address addr)
{
  m_ipv4Address.at (index) = addr;
}

Ipv4Address LbsrOptionRrepHeader::GetNodeAddress (uint8_t index) const
{
  return m_ipv4Address.at (index);
}

Ipv4Address LbsrOptionRrepHeader::GetTargetAddress (std::vector<Ipv4Address> ipv4Address) const
{
  return m_ipv4Address.at (ipv4Address.size () - 1);
}

void LbsrOptionRrepHeader::Print (std::ostream &os) const
{
  os << "( type = " << (uint32_t)GetType () << " length = " << (uint32_t)GetLength () << "";

  for (std::vector<Ipv4Address>::const_iterator it = m_ipv4Address.begin (); it != m_ipv4Address.end (); it++)
    {
      os << *it << " ";
    }

  os << ")";
}

uint32_t LbsrOptionRrepHeader::GetSerializedSize () const
{
	//must change here
 // return (4 + m_ipv4Address.size () * 4);
	//return (16 + m_ipv4Address.size () * 4);
	return (7 + m_ipv4Address.size () * 4);
}

void LbsrOptionRrepHeader::Serialize (Buffer::Iterator start) const
{
  Buffer::Iterator i = start;
  uint8_t buff[4];

  i.WriteU8 (GetType ());
  i.WriteU8 (GetLength ());
  i.WriteU8 (0);
  i.WriteU8 (0);

  //uint16_t addr = (uint16_t)addr_num;
  //i.WriteU16(addr);
  uint8_t addr = addr_num;
  i.WriteU8(addr);

  //uint64_t ba = (uint64_t)nodeBattery;
  //std::cout << "Serialize 1 battery is " << ba << "\n";
  //i.WriteHtonU64 (ba);
  uint16_t ba = nodeBattery;
  i.WriteU16(ba);
  //std::cout << "Serialize 2 battery is " << i.ReadU32() << "\n";

  for (VectorIpv4Address_t::const_iterator it = m_ipv4Address.begin (); it != m_ipv4Address.end (); it++)
    {
      it->Serialize (buff);
      i.Write (buff, 4);
    }
}

uint32_t LbsrOptionRrepHeader::Deserialize (Buffer::Iterator start)
{
  Buffer::Iterator i = start;
  uint8_t buff[4];

  SetType (i.ReadU8 ());
  SetLength (i.ReadU8 ());
  i.ReadU8 ();
  i.ReadU8 ();
  addr_num = i.ReadU8();
  SetBatteryDe(i.ReadNtohU16());
  uint8_t index = 0;
  for (std::vector<Ipv4Address>::iterator it = m_ipv4Address.begin (); it != m_ipv4Address.end (); it++)
    {
      i.Read (buff, 4);
      m_address = it->Deserialize (buff);
      SetNodeAddress (index, m_address);
      ++index;
    }

  return GetSerializedSize ();
}

LbsrOptionHeader::Alignment LbsrOptionRrepHeader::GetAlignment () const
{
  Alignment retVal = { 4, 0 };
  return retVal;
}




//Lstop

NS_OBJECT_ENSURE_REGISTERED (LbsrOptionLstopHeader);

TypeId LbsrOptionLstopHeader::GetTypeId ()
{
  static TypeId tid = TypeId ("ns3::lbsr::LbsrOptionLstopHeader")
    .AddConstructor<LbsrOptionLstopHeader> ()
    .SetParent<LbsrOptionHeader> ()
    .SetGroupName ("Lbsr")
  ;
  return tid;
}

TypeId LbsrOptionLstopHeader::GetInstanceTypeId () const
{
  return GetTypeId ();
}

LbsrOptionLstopHeader::LbsrOptionLstopHeader ()
  : m_ipv4Address (0)
{
  SetType (5);
  SetLength (2 + m_ipv4Address.size () * 4);
}

LbsrOptionLstopHeader::~LbsrOptionLstopHeader ()
{
}

void LbsrOptionLstopHeader::SetNumberAddress (uint8_t n)
{
  m_ipv4Address.clear ();
  m_ipv4Address.assign (n, Ipv4Address ());
}

void LbsrOptionLstopHeader::SetNodesAddress (std::vector<Ipv4Address> ipv4Address)
{
  m_ipv4Address = ipv4Address;
  SetLength (2 + m_ipv4Address.size () * 4);
}

std::vector<Ipv4Address> LbsrOptionLstopHeader::GetNodesAddress () const
{
  return m_ipv4Address;
}

void LbsrOptionLstopHeader::SetNodeAddress (uint8_t index, Ipv4Address addr)
{
  m_ipv4Address.at (index) = addr;
}

Ipv4Address LbsrOptionLstopHeader::GetNodeAddress (uint8_t index) const
{
  return m_ipv4Address.at (index);
}

Ipv4Address LbsrOptionLstopHeader::GetTargetAddress (std::vector<Ipv4Address> ipv4Address) const
{
  return m_ipv4Address.at (ipv4Address.size () - 1);
}

void LbsrOptionLstopHeader::Print (std::ostream &os) const
{
  os << "( type = " << (uint32_t)GetType () << " length = " << (uint32_t)GetLength () << "";

  for (std::vector<Ipv4Address>::const_iterator it = m_ipv4Address.begin (); it != m_ipv4Address.end (); it++)
    {
      os << *it << " ";
    }

  os << ")";
}

uint32_t LbsrOptionLstopHeader::GetSerializedSize () const
{
  return 4 + m_ipv4Address.size () * 4;
}

void LbsrOptionLstopHeader::Serialize (Buffer::Iterator start) const
{
  Buffer::Iterator i = start;
  uint8_t buff[4];

  i.WriteU8 (GetType ());
  i.WriteU8 (GetLength ());
  i.WriteU8 (0);
  i.WriteU8 (0);

  for (VectorIpv4Address_t::const_iterator it = m_ipv4Address.begin (); it != m_ipv4Address.end (); it++)
    {
      it->Serialize (buff);
      i.Write (buff, 4);
    }
}

uint32_t LbsrOptionLstopHeader::Deserialize (Buffer::Iterator start)
{
  Buffer::Iterator i = start;
  uint8_t buff[4];

  SetType (i.ReadU8 ());
  SetLength (i.ReadU8 ());
  i.ReadU8 ();
  i.ReadU8 ();

  uint8_t index = 0;
  for (std::vector<Ipv4Address>::iterator it = m_ipv4Address.begin (); it != m_ipv4Address.end (); it++)
    {
      i.Read (buff, 4);
      m_address = it->Deserialize (buff);
      SetNodeAddress (index, m_address);
      ++index;
    }

  return GetSerializedSize ();
}

LbsrOptionHeader::Alignment LbsrOptionLstopHeader::GetAlignment () const
{
  Alignment retVal = { 4, 0 };
  return retVal;
}




NS_OBJECT_ENSURE_REGISTERED (LbsrOptionSRHeader);

TypeId LbsrOptionSRHeader::GetTypeId ()
{
  static TypeId tid = TypeId ("ns3::lbsr::LbsrOptionSRHeader")
    .AddConstructor<LbsrOptionSRHeader> ()
    .SetParent<LbsrOptionHeader> ()
    .SetGroupName ("Lbsr")
  ;
  return tid;
}

TypeId LbsrOptionSRHeader::GetInstanceTypeId () const
{
  return GetTypeId ();
}

LbsrOptionSRHeader::LbsrOptionSRHeader ()
  : m_segmentsLeft (0),
    m_ipv4Address (0)
{
  SetType (96);
  SetLength (2 + m_ipv4Address.size () * 4);
}

LbsrOptionSRHeader::~LbsrOptionSRHeader ()
{
}

void LbsrOptionSRHeader::SetSegmentsLeft (uint8_t segmentsLeft)
{
  m_segmentsLeft = segmentsLeft;
}

uint8_t LbsrOptionSRHeader::GetSegmentsLeft () const
{
  return m_segmentsLeft;
}

void LbsrOptionSRHeader::SetSalvage (uint8_t salvage)
{
  m_salvage = salvage;
}

uint8_t LbsrOptionSRHeader::GetSalvage () const
{
  return m_salvage;
}

void LbsrOptionSRHeader::SetNumberAddress (uint8_t n)
{
  m_ipv4Address.clear ();
  m_ipv4Address.assign (n, Ipv4Address ());
}

void LbsrOptionSRHeader::SetNodesAddress (std::vector<Ipv4Address> ipv4Address)
{
  m_ipv4Address = ipv4Address;
  SetLength (2 + m_ipv4Address.size () * 4);
}

std::vector<Ipv4Address> LbsrOptionSRHeader::GetNodesAddress () const
{
  return m_ipv4Address;
}

void LbsrOptionSRHeader::SetNodeAddress (uint8_t index, Ipv4Address addr)
{
  m_ipv4Address.at (index) = addr;
}

Ipv4Address LbsrOptionSRHeader::GetNodeAddress (uint8_t index) const
{
  return m_ipv4Address.at (index);
}

uint8_t LbsrOptionSRHeader::GetNodeListSize () const
{
  return m_ipv4Address.size ();
}

void LbsrOptionSRHeader::Print (std::ostream &os) const
{
  os << "( type = " << (uint32_t)GetType () << " length = " << (uint32_t)GetLength () << "";

  for (std::vector<Ipv4Address>::const_iterator it = m_ipv4Address.begin (); it != m_ipv4Address.end (); it++)
    {
      os << *it << " ";
    }

  os << ")";
}

uint32_t LbsrOptionSRHeader::GetSerializedSize () const
{
  return 4 + m_ipv4Address.size () * 4;
}

void LbsrOptionSRHeader::Serialize (Buffer::Iterator start) const
{
  Buffer::Iterator i = start;
  uint8_t buff[4];

  i.WriteU8 (GetType ());
  i.WriteU8 (GetLength ());
  i.WriteU8 (m_salvage);
  i.WriteU8 (m_segmentsLeft);

  for (VectorIpv4Address_t::const_iterator it = m_ipv4Address.begin (); it != m_ipv4Address.end (); it++)
    {
      it->Serialize (buff);
      i.Write (buff, 4);
    }
}

uint32_t LbsrOptionSRHeader::Deserialize (Buffer::Iterator start)
{
  Buffer::Iterator i = start;
  uint8_t buff[4];

  SetType (i.ReadU8 ());
  SetLength (i.ReadU8 ());
  m_salvage = i.ReadU8 ();
  m_segmentsLeft = i.ReadU8 ();

  uint8_t index = 0;
  for (std::vector<Ipv4Address>::iterator it = m_ipv4Address.begin (); it != m_ipv4Address.end (); it++)
    {
      i.Read (buff, 4);
      m_address = it->Deserialize (buff);
      SetNodeAddress (index, m_address);
      ++index;
    }

  return GetSerializedSize ();
}

LbsrOptionHeader::Alignment LbsrOptionSRHeader::GetAlignment () const
{
  Alignment retVal = { 4, 0 };
  return retVal;
}

NS_OBJECT_ENSURE_REGISTERED (LbsrOptionRerrHeader);

TypeId LbsrOptionRerrHeader::GetTypeId ()
{
  static TypeId tid = TypeId ("ns3::lbsr::LbsrOptionRerrHeader")
    .AddConstructor<LbsrOptionRerrHeader> ()
    .SetParent<LbsrOptionHeader> ()
    .SetGroupName ("Lbsr")
  ;
  return tid;
}

TypeId LbsrOptionRerrHeader::GetInstanceTypeId () const
{
  return GetTypeId ();
}

LbsrOptionRerrHeader::LbsrOptionRerrHeader ()
  : m_errorType (0),
    m_salvage (0),
    m_errorLength (4)
{
  SetType (3);
  SetLength (18);
}

LbsrOptionRerrHeader::~LbsrOptionRerrHeader ()
{
}

void LbsrOptionRerrHeader::SetErrorType (uint8_t errorType)
{
  m_errorType = errorType;
}

uint8_t LbsrOptionRerrHeader::GetErrorType () const
{
  return m_errorType;
}

void LbsrOptionRerrHeader::SetSalvage (uint8_t salvage)
{
  m_salvage = salvage;
}

uint8_t LbsrOptionRerrHeader::GetSalvage () const
{
  return m_salvage;
}

void LbsrOptionRerrHeader::SetErrorSrc (Ipv4Address errorSrcAddress)
{
  m_errorSrcAddress = errorSrcAddress;
}

Ipv4Address LbsrOptionRerrHeader::GetErrorSrc () const
{
  return m_errorSrcAddress;
}

void LbsrOptionRerrHeader::SetErrorDst (Ipv4Address errorDstAddress)
{
  m_errorDstAddress = errorDstAddress;
}

Ipv4Address LbsrOptionRerrHeader::GetErrorDst () const
{
  return m_errorDstAddress;
}

void LbsrOptionRerrHeader::Print (std::ostream &os) const
{
  os << "( type = " << (uint32_t)GetType () << " length = " << (uint32_t)GetLength ()
     << " errorType = " << (uint32_t)m_errorType << " salvage = " << (uint32_t)m_salvage
     << " error source = " << m_errorSrcAddress << " error dst = " << m_errorDstAddress << " )";

}

uint32_t LbsrOptionRerrHeader::GetSerializedSize () const
{
  return 20;
}

void LbsrOptionRerrHeader::Serialize (Buffer::Iterator start) const
{
  Buffer::Iterator i = start;

  i.WriteU8 (GetType ());
  i.WriteU8 (GetLength ());
  i.WriteU8 (m_errorType);
  i.WriteU8 (m_salvage);
  WriteTo (i, m_errorSrcAddress);
  WriteTo (i, m_errorDstAddress);
  i.Write (m_errorData.Begin (), m_errorData.End ());
}

uint32_t LbsrOptionRerrHeader::Deserialize (Buffer::Iterator start)
{
  Buffer::Iterator i = start;

  SetType (i.ReadU8 ());
  SetLength (i.ReadU8 ());
  m_errorType = i.ReadU8 ();
  m_salvage = i.ReadU8 ();
  ReadFrom (i, m_errorSrcAddress);
  ReadFrom (i, m_errorDstAddress);

  m_errorData = Buffer ();
  m_errorData.AddAtEnd (m_errorLength);
  Buffer::Iterator dataStart = i;
  i.Next (m_errorLength);
  Buffer::Iterator dataEnd = i;
  m_errorData.Begin ().Write (dataStart, dataEnd);

  return GetSerializedSize ();
}

LbsrOptionHeader::Alignment LbsrOptionRerrHeader::GetAlignment () const
{
  Alignment retVal = { 4, 0 };
  return retVal;
}

NS_OBJECT_ENSURE_REGISTERED (LbsrOptionRerrUnreachHeader);

TypeId LbsrOptionRerrUnreachHeader::GetTypeId ()
{
  static TypeId tid = TypeId ("ns3::lbsr::LbsrOptionRerrUnreachHeader")
    .AddConstructor<LbsrOptionRerrUnreachHeader> ()
    .SetParent<LbsrOptionRerrHeader> ()
    .SetGroupName ("Lbsr")
  ;
  return tid;
}

TypeId LbsrOptionRerrUnreachHeader::GetInstanceTypeId () const
{
  return GetTypeId ();
}

LbsrOptionRerrUnreachHeader::LbsrOptionRerrUnreachHeader ()
  : m_salvage (0)
{
  SetType (3);
  SetLength (18);
  SetErrorType (1);
}

LbsrOptionRerrUnreachHeader::~LbsrOptionRerrUnreachHeader ()
{
}

void LbsrOptionRerrUnreachHeader::SetSalvage (uint8_t salvage)
{
  m_salvage = salvage;
}

uint8_t LbsrOptionRerrUnreachHeader::GetSalvage () const
{
  return m_salvage;
}

void LbsrOptionRerrUnreachHeader::SetErrorSrc (Ipv4Address errorSrcAddress)
{
  m_errorSrcAddress = errorSrcAddress;
}

Ipv4Address LbsrOptionRerrUnreachHeader::GetErrorSrc () const
{
  return m_errorSrcAddress;
}

void LbsrOptionRerrUnreachHeader::SetErrorDst (Ipv4Address errorDstAddress)
{
  m_errorDstAddress = errorDstAddress;
}

Ipv4Address LbsrOptionRerrUnreachHeader::GetErrorDst () const
{
  return m_errorDstAddress;
}

void LbsrOptionRerrUnreachHeader::SetUnreachNode (Ipv4Address unreachNode)
{
  m_unreachNode = unreachNode;
}

Ipv4Address LbsrOptionRerrUnreachHeader::GetUnreachNode () const
{
  return m_unreachNode;
}

void LbsrOptionRerrUnreachHeader::SetOriginalDst (Ipv4Address originalDst)
{
  m_originalDst = originalDst;
}

Ipv4Address LbsrOptionRerrUnreachHeader::GetOriginalDst () const
{
  return m_originalDst;
}

void LbsrOptionRerrUnreachHeader::Print (std::ostream &os) const
{
  os << "( type = " << (uint32_t)GetType () << " length = " << (uint32_t)GetLength ()
     << " errorType = " << (uint32_t)m_errorType << " salvage = " << (uint32_t)m_salvage
     << " error source = " << m_errorSrcAddress << " error dst = " << m_errorDstAddress
     << " unreach node = " <<  m_unreachNode << " )";
}

uint32_t LbsrOptionRerrUnreachHeader::GetSerializedSize () const
{
  return 20;
}

void LbsrOptionRerrUnreachHeader::Serialize (Buffer::Iterator start) const
{
  Buffer::Iterator i = start;

  i.WriteU8 (GetType ());
  i.WriteU8 (GetLength ());
  i.WriteU8 (GetErrorType ());
  i.WriteU8 (m_salvage);
  WriteTo (i, m_errorSrcAddress);
  WriteTo (i, m_errorDstAddress);
  WriteTo (i, m_unreachNode);
  WriteTo (i, m_originalDst);
}

uint32_t LbsrOptionRerrUnreachHeader::Deserialize (Buffer::Iterator start)
{
  Buffer::Iterator i = start;

  SetType (i.ReadU8 ());
  SetLength (i.ReadU8 ());
  SetErrorType (i.ReadU8 ());
  m_salvage = i.ReadU8 ();
  ReadFrom (i, m_errorSrcAddress);
  ReadFrom (i, m_errorDstAddress);
  ReadFrom (i, m_unreachNode);
  ReadFrom (i, m_originalDst);

  return GetSerializedSize ();
}

LbsrOptionHeader::Alignment LbsrOptionRerrUnreachHeader::GetAlignment () const
{
  Alignment retVal = { 4, 0 };
  return retVal;
}

NS_OBJECT_ENSURE_REGISTERED (LbsrOptionRerrUnsupportHeader);

TypeId LbsrOptionRerrUnsupportHeader::GetTypeId ()
{
  static TypeId tid = TypeId ("ns3::lbsr::LbsrOptionRerrUnsupportHeader")
    .AddConstructor<LbsrOptionRerrUnsupportHeader> ()
    .SetParent<LbsrOptionRerrHeader> ()
    .SetGroupName ("Lbsr")
  ;
  return tid;
}

TypeId LbsrOptionRerrUnsupportHeader::GetInstanceTypeId () const
{
  return GetTypeId ();
}

LbsrOptionRerrUnsupportHeader::LbsrOptionRerrUnsupportHeader ()
  : m_salvage (0)
{
  SetType (3);
  SetLength (14);
  SetErrorType (3);
}

LbsrOptionRerrUnsupportHeader::~LbsrOptionRerrUnsupportHeader ()
{
}

void LbsrOptionRerrUnsupportHeader::SetSalvage (uint8_t salvage)
{
  m_salvage = salvage;
}

uint8_t LbsrOptionRerrUnsupportHeader::GetSalvage () const
{
  return m_salvage;
}

void LbsrOptionRerrUnsupportHeader::SetErrorSrc (Ipv4Address errorSrcAddress)
{
  m_errorSrcAddress = errorSrcAddress;
}

Ipv4Address LbsrOptionRerrUnsupportHeader::GetErrorSrc () const
{
  return m_errorSrcAddress;
}

void LbsrOptionRerrUnsupportHeader::SetErrorDst (Ipv4Address errorDstAddress)
{
  m_errorDstAddress = errorDstAddress;
}

Ipv4Address LbsrOptionRerrUnsupportHeader::GetErrorDst () const
{
  return m_errorDstAddress;
}

void LbsrOptionRerrUnsupportHeader::SetUnsupported (uint16_t unsupport)
{
  m_unsupport = unsupport;
}

uint16_t LbsrOptionRerrUnsupportHeader::GetUnsupported () const
{
  return m_unsupport;
}

void LbsrOptionRerrUnsupportHeader::Print (std::ostream &os) const
{
  os << "( type = " << (uint32_t)GetType () << " length = " << (uint32_t)GetLength ()
     << " errorType = " << (uint32_t)m_errorType << " salvage = " << (uint32_t)m_salvage
     << " error source = " << m_errorSrcAddress << " error dst = " << m_errorDstAddress
     << " unsupported option = " <<  m_unsupport << " )";

}

uint32_t LbsrOptionRerrUnsupportHeader::GetSerializedSize () const
{
  return 16;
}

void LbsrOptionRerrUnsupportHeader::Serialize (Buffer::Iterator start) const
{
  Buffer::Iterator i = start;

  i.WriteU8 (GetType ());
  i.WriteU8 (GetLength ());
  i.WriteU8 (GetErrorType ());
  i.WriteU8 (m_salvage);
  WriteTo (i, m_errorSrcAddress);
  WriteTo (i, m_errorDstAddress);
  i.WriteU16 (m_unsupport);

}

uint32_t LbsrOptionRerrUnsupportHeader::Deserialize (Buffer::Iterator start)
{
  Buffer::Iterator i = start;

  SetType (i.ReadU8 ());
  SetLength (i.ReadU8 ());
  SetErrorType (i.ReadU8 ());
  m_salvage = i.ReadU8 ();
  ReadFrom (i, m_errorSrcAddress);
  ReadFrom (i, m_errorDstAddress);
  m_unsupport = i.ReadU16 ();

  return GetSerializedSize ();
}

LbsrOptionHeader::Alignment LbsrOptionRerrUnsupportHeader::GetAlignment () const
{
  Alignment retVal = { 4, 0 };
  return retVal;
}

NS_OBJECT_ENSURE_REGISTERED (LbsrOptionAckReqHeader);

TypeId LbsrOptionAckReqHeader::GetTypeId ()
{
  static TypeId tid = TypeId ("ns3::lbsr::LbsrOptionAckReqHeader")
    .AddConstructor<LbsrOptionAckReqHeader> ()
    .SetParent<LbsrOptionHeader> ()
    .SetGroupName ("Lbsr")
  ;
  return tid;
}

TypeId LbsrOptionAckReqHeader::GetInstanceTypeId () const
{
  return GetTypeId ();
}

LbsrOptionAckReqHeader::LbsrOptionAckReqHeader ()
  : m_identification (0)

{
  SetType (160);
  SetLength (2);
}

LbsrOptionAckReqHeader::~LbsrOptionAckReqHeader ()
{
}

void LbsrOptionAckReqHeader::SetAckId (uint16_t identification)
{
  m_identification = identification;
}

uint16_t LbsrOptionAckReqHeader::GetAckId () const
{
  return m_identification;
}

void LbsrOptionAckReqHeader::Print (std::ostream &os) const
{
  os << "( type = " << (uint32_t)GetType () << " length = " << (uint32_t)GetLength ()
     << " id = " << m_identification << " )";
}

uint32_t LbsrOptionAckReqHeader::GetSerializedSize () const
{
  return 4;
}

void LbsrOptionAckReqHeader::Serialize (Buffer::Iterator start) const
{
  Buffer::Iterator i = start;

  i.WriteU8 (GetType ());
  i.WriteU8 (GetLength ());
  i.WriteU16 (m_identification);
}

uint32_t LbsrOptionAckReqHeader::Deserialize (Buffer::Iterator start)
{
  Buffer::Iterator i = start;

  SetType (i.ReadU8 ());
  SetLength (i.ReadU8 ());
  m_identification = i.ReadU16 ();

  return GetSerializedSize ();
}

LbsrOptionHeader::Alignment LbsrOptionAckReqHeader::GetAlignment () const
{
  Alignment retVal = { 4, 0 };
  return retVal;
}

NS_OBJECT_ENSURE_REGISTERED (LbsrOptionAckHeader);

TypeId LbsrOptionAckHeader::GetTypeId ()
{
  static TypeId tid = TypeId ("ns3::lbsr::LbsrOptionAckHeader")
    .AddConstructor<LbsrOptionAckHeader> ()
    .SetParent<LbsrOptionHeader> ()
    .SetGroupName ("Lbsr")
  ;
  return tid;
}

TypeId LbsrOptionAckHeader::GetInstanceTypeId () const
{
  return GetTypeId ();
}

LbsrOptionAckHeader::LbsrOptionAckHeader ()
  :    m_identification (0)
{
  SetType (32);
  SetLength (10);
}

LbsrOptionAckHeader::~LbsrOptionAckHeader ()
{
}

void LbsrOptionAckHeader::SetAckId (uint16_t identification)
{
  m_identification = identification;
}

uint16_t LbsrOptionAckHeader::GetAckId () const
{
  return m_identification;
}

void LbsrOptionAckHeader::SetRealSrc (Ipv4Address realSrcAddress)
{
  m_realSrcAddress = realSrcAddress;
}

Ipv4Address LbsrOptionAckHeader::GetRealSrc () const
{
  return m_realSrcAddress;
}

void LbsrOptionAckHeader::SetRealDst (Ipv4Address realDstAddress)
{
  m_realDstAddress = realDstAddress;
}

Ipv4Address LbsrOptionAckHeader::GetRealDst () const
{
  return m_realDstAddress;
}

void LbsrOptionAckHeader::Print (std::ostream &os) const
{
  os << "( type = " << (uint32_t)GetType () << " length = " << (uint32_t)GetLength ()
     << " id = " << m_identification << " real src = " << m_realSrcAddress
     << " real dst = " << m_realDstAddress << " )";

}

uint32_t LbsrOptionAckHeader::GetSerializedSize () const
{
  return 12;
}

void LbsrOptionAckHeader::Serialize (Buffer::Iterator start) const
{
  Buffer::Iterator i = start;

  i.WriteU8 (GetType ());
  i.WriteU8 (GetLength ());
  i.WriteU16 (m_identification);
  WriteTo (i, m_realSrcAddress);
  WriteTo (i, m_realDstAddress);
}

uint32_t LbsrOptionAckHeader::Deserialize (Buffer::Iterator start)
{
  Buffer::Iterator i = start;

  SetType (i.ReadU8 ());
  SetLength (i.ReadU8 ());
  m_identification = i.ReadU16 ();
  ReadFrom (i, m_realSrcAddress);
  ReadFrom (i, m_realDstAddress);

  return GetSerializedSize ();
}

LbsrOptionHeader::Alignment LbsrOptionAckHeader::GetAlignment () const
{
  Alignment retVal = { 4, 0 };
  return retVal;
}
} /* namespace lbsr */
} /* namespace ns3 */
