# Ns-3.29でのLBSRとその改善の作成

Ns-3.29でLBSRを作成したものである. 本来のLBSRは違いがあるのでその部分は注意部分で記述する

## 使用方法

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### 使用環境

* Ubuntu18.04.02
* Ns-3 (v3.29)

### インストール

* Step 1: Ns3のダウンロード, [Installation - Nsnam - ns-3](https://www.nsnam.org/wiki/Installation)このサイトを参考にしてインストールしてください

* Step 2: Ns-3/srcにlbsrというファイルを作成し, lbsrの中にlbsr_origin又はlbsr_teianをコピペしてください

* Step 3: Ns-3/scratchにMyLbsrをコピペする

## 実行
実行結果として表示されるのは経路と到達率と平均バッテリー残量である
提案方式の場合は所得した経路をすべて表示する
## 実験例

```
createnodes
CreateDevices
installInternetStack
sink is 0x5580e70be710
source is 10.1.1.1
test
Installapplication
aodvToLbsr_Start
Starting simulation for 130 s ...
LbsrRouting::Start mainAddress
addr is 10.1.1.1
LbsrRouting::Start mainAddress
addr is 10.1.1.2
LbsrRouting::Start mainAddress
addr is 10.1.1.3
LbsrRouting::Start mainAddress
addr is 10.1.1.4
LbsrRouting::Start mainAddress
addr is 10.1.1.5
LbsrRouting::Start mainAddress
addr is 10.1.1.6
LbsrRouting::Start mainAddress
addr is 10.1.1.7
LbsrRouting::Start mainAddress
addr is 10.1.1.8
LbsrRouting::Start mainAddress
addr is 10.1.1.9
LbsrRouting::Start mainAddress
addr is 10.1.1.10
LbsrRouting::Start mainAddress
addr is 10.1.1.11
LbsrRouting::Start mainAddress
addr is 10.1.1.12
LbsrRouting::Start mainAddress
addr is 10.1.1.13
LbsrRouting::Start mainAddress
addr is 10.1.1.14
LbsrRouting::Start mainAddress
addr is 10.1.1.15
LbsrRouting::Start mainAddress
addr is 10.1.1.16
LbsrRouting::Start mainAddress
addr is 10.1.1.17
LbsrRouting::Start mainAddress
addr is 10.1.1.18
LbsrRouting::Start mainAddress
addr is 10.1.1.19
LbsrRouting::Start mainAddress
addr is 10.1.1.20
LbsrRouting::Start mainAddress
addr is 10.1.1.21
LbsrRouting::Start mainAddress
addr is 10.1.1.22
LbsrRouting::Start mainAddress
addr is 10.1.1.23
LbsrRouting::Start mainAddress
addr is 10.1.1.24
LbsrRouting::Start mainAddress
addr is 10.1.1.25
LbsrRouting::Start mainAddress
addr is 10.1.1.26
LbsrRouting::Start mainAddress
addr is 10.1.1.27
LbsrRouting::Start mainAddress
addr is 10.1.1.28
LbsrRouting::Start mainAddress
addr is 10.1.1.29
LbsrRouting::Start mainAddress
addr is 10.1.1.30
LbsrRouting::SendInitialRequest source 10.1.1.30 destination 10.1.1.1 protocol 17 nowtime 101
sendFlag is false

LbsrRouting::RouteRequestTimerExpire m_mainAddress 10.1.1.30
sendFlag is false

des get Lreq
LbsrRouting::RouteRequestTimerExpire m_mainAddress 10.1.1.30
sendFlag is false

LbsrRouting::RouteRequestTimerExpire m_mainAddress 10.1.1.30
des get Lconf
BroadCast 32 Unicast 366


 SendRouteNode 

Create Route Time is 107.04
------Create S->D->S ----------
changeRoute is10.1.1.30
changeRoute is10.1.1.26
changeRoute is10.1.1.2
changeRoute is10.1.1.19
changeRoute is10.1.1.1
changeRoute is10.1.1.20
changeRoute is10.1.1.30
--------------------------------
AverageBattery is 112.398
nexthop = 10.1.1.20 ipv4address = 10.1.1.30
BroadCast is 32 UniCast is 770
AverageBattery is 94.1814
getpid() 19706
total sink packet is 1408bytes
total source packet is 1920bytes
total packet reachability is 73.3333%
simulation stop
```
## 注意事項
今回のLBSRは送信元と宛先の1ペアのみとなっている. また, 現状宛先はノードの1番目になっている

## 著者

* ネットワークシステム研究室 出羽健二
* 2020/2/12
## Acknowledgments

* Hat tip to anyone whose code was used
* Inspiration
* etc

